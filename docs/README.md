
# 文件資料

教程文檔中有asciidoc描述。

```
$ ls src/main/asciidoc/
index.adoc  images/     ...
```

該maven文檔的HTML版本可以通過以下方式生成：

```
$ mvn
$ ls target/generated-docs/
index.html  images/     ...
```