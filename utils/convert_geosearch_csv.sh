#!/usr/bin/env bash
if [ $? != 0 ]; then
  yum install -y jq
fi
jq -s -R '[[split("\n")[]|split(",")|select(length==8)]|{h:.[0],v:.[1:][]}|[.h, .v]|[transpose[]|{key:.[0],value:.[1]}]|from_entries|{put:("id:twmask:twmask::"+.pos),fields:.}]' $1
