# vespa 教程

搜尋引擎 [Vespa](http://vespa.ai/) 的環境教程的示例代碼。

請同時參考[教程材料](http://172.21.4.27/vespa-tutorial)。

2020.2.18 新增 [geosearch教程](http://172.21.4.27/vespa-tutorial/geosearch.html)

## 版權

此示例代碼在MIT許可下提供。
檢查LICENSE文件以獲取詳細信息。

## 準備工作

要運行本教程，您需要以下兩個軟體。

* [`docker`](https://www.docker.com/)
* [`docker-compose`](https://docs.docker.com/compose/)

請事先將這兩個安裝在執行環境中。

### 在CentOS7上的示例

```bash
// install docker
$ sudo yum install docker
$ sudo systemctl enable docker
$ sudo systemctl start docker

// install dokcer-compose
$ sudo curl -L https://github.com/docker/compose/releases/download/1.18.0/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
$ sudo chmod +x /usr/local/bin/docker-compose
```

## 快速上手

如果要快速檢查boot.shVespa 的運行情況，可以使用以下方法構建Vespa集群
（啟動Vespa集群大約需要8 GB的內存，如果要單獨啟動，請參閱教程材料）請）。

```bash
// please move to vespa-tutorial directory first
$ cd vespa-tutorial/

// start Vespa cluster
$ ./boot.sh start

// stop Vespa cluster
$ ./boot.sh stop
```

推出的Vespa 8080將接受按端口搜索。

```bash
$ curl 'http://localhost:8080/search/?lang=ja&query=入門'
```
